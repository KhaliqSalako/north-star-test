### Log in

* Endpoint path: /token
* Endpoint method: POST

* Request shape (form):
  * username: "string"
  * password: "string"

* Response: Account information and a token
* Response shape (JSON):
    ```json
    {
      "owner_account": {
        «key»: type»,
      },
      "token": 'string'
    }
    ```

### sign up

* Endpoint path: /token
* Endpoint method: POST
    ```json
  {
    "refreshToken": "string",
    "registration": {
      "accountId": "10000000-0000-0002-0000-000000000001",
      "data": {
        "displayName": "string",
      },
      "id": "00000000-0000-0002-0000-000000000000",
      "preferredLanguages": [
        "en",
        "fr"
      ],
      "timezone": "America/Chicago",
      "username": "string",
    },
    "token": "string",
    "user": {
      "birthDate": "1976-05-30",
      "data": {
        "displayName": "string",
      },
      "email": "example@icloud.com",
      "firstName": "string",
      "fullName": "string",
      "id": "00000000-0000-0001-0000-000000000000",
      "accountImageUrl": ".jpg",
      "lastName": "string",
      "middleName": "string",
      "mobilePhone": "integer",
      "timezone": "America/Chicago",
      "twoFactor": {
            "id": "35VW",
            "method": "authenticator"
          },
          {
            "id": "V7SH",
            "method": "sms",
            "mobilePhone": "integer"
          },
          {
            "email": "example@icloud.com",
            "id": "7K2G",
            "method": "email"
          }
        ]
      },
      "usernameStatus": "ACTIVE",
      "username": "string",
      "verified": true
    }
  }
    ```

### Log out

* Endpoint path: /token
* Endpoint method: DELETE

* Headers:
  * Authorization: Bearer token

* Response: Always true
* Response shape (JSON):
    ```json
    true
    ```
