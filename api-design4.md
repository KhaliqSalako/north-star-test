### Get Event Details

- Endpoint path: /itinerary/<id>/event/<id>/
- Endpoint method: GET

- Headers:

  - Authorization: Bearer token

- Response: Event Detail
- Response shape:

  ```json
  {
    "event": [
      {
        "id": number,
        "name": string,
        "location": {
          "name": string,
          "details": text,
          "picture_url": string,
          "geo_location": string,
        },
        "start_time": number,
        "end_time": number,
        "details": text,
        "itinerary": {
          "date": date,
          "start_location": string,
        }
      }

    ]
  }
  ```

### Create new Event

- Endpoint path: /itinerary/<id>/event/
- Endpoint method: POST

- Headers:

  - Authorization: Bearer token

- Request body:
  ```json
  {
    "event": [
      {
        "name": string,
        "location": text,
        "start_time": number,
        "end_time": number,
        "details": text
      }
    ]
  }
  ```
- Response: An indication of success or failure
- Response shape:
  ```json
  {
    "success": boolean,
    "message": string
  }
  ```

### Edit Event

- Endpoint path: /itinerary/<id>/event/<id>/
- Endpoint method: POST

- Headers:

  - Authorization: Bearer token

- Request body:
  ```json
  {
    "event": [
      {
        "name": string,
        "location": text,
        "start_time": number,
        "end_time": number,
        "details": text
      }
    ]
  }
  ```
- Response: An indication of success or failure
- Response shape:
  ```json
  {
    "success": boolean,
    "message": string
  }
  ```

### Get Location Search

- Endpoint path: /itinerary/<id>/event/<id>/
- Endpoint method: GET
- Query parameters:

  - Search: Input

- Headers:

  - Authorization: Bearer token

- Request shape (JSON):

  ```json
        "location": {
          "name": string,
          "details": text,
          "picture_url": string,
          "geo_location": string,
        },
  ```

- Response: Search Maps API for location

- Response shape (JSON):
  ```json
        "location": {
          "name": string,
          "details": text,
          "picture_url": string,
          "geo_location": string,
        },
  ```
