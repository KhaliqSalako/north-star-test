### List of Trips

* Endpoint path: /trips
* Endpoint method: GET

* Headers:
    * Authorization: Login Required

* Response: A list of all trips
* Response shape:
    ```json
    {
        "trips": [
            {
              "name" : string,
              "start_date" : date field,
              "end_date" : date field,
              "picture_url" : url
            }
        ]
    }
    ```

### Create a Trip

* Endpoint path: /trips/create
* Endpoint method: POST

* Headers:
    * Authorization: Login Required

* Response: Create a new trip
* Response shape:
    ```json
    {
        "trips": [
            {
              "name" : string,
              "start_date" : date field,
              "end_date" : date field,
              "picture_url" : url
            }
        ]
    }
    ```

### Delete a Trip

* Endpoint path: /trips/delete
* Endpoint method: DELETE

* Headers:
    * Authorization: Login Required

* Response: Delete a Trip
* Response shape:
    ```json
    {
        "trips": [
            {
              "name" : string,
              "start_date" : date field,
              "end_date" : date field,
              "picture_url" : url
            }
        ]
    }
    ```

### Edit a Trip

* Endpoint path: /trips/edit
* Endpoint method: PUT

* Headers:
    * Authorization: Login Required

* Response: Edit a Trip
* Response shape:
    ```json
    {
        "trips": [
            {
              "name" : string,
              "start_date" : date field,
              "end_date" : date field,
              "picture_url" : url
            }
        ]
    }
    ```
