
* Response shape for get itinerary detail
    ```json
    {
      "itinerary": [
        {
          "id": number,
          "date": DateField,
        }
      ]
    }
    ```


* Response shape for delete event 
    ```json
    {
      "event": [
        {
          "id": number,
          "name": string,
          "address": string
        }
      ]
    }
    ```

* Response shape for Post event detail
    ```json
    {
      "event": [
        {
          "id": number,
          "name": string,
          "address": string,
          "start_time": TimeField,
          "end_time": TimeField,
          "details": string
        }
      ]
    }
    ```

